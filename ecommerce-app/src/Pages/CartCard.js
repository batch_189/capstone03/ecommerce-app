import React, { useState, useEffect } from "react";
import { Button, Container } from "react-bootstrap";
import Product01 from "../assets/products/product_01.jpg";
import "./CartCard.css";

const ViewCart = () => {
  const [products, setProducts] = useState([]);
  const [totalAmount, setTotalAmount] = useState(0);
  useEffect(() => {
    console.log("nyay");
    fetch("http://localhost:5000/orders/current", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        console.log("------asd-");
        console.log(data);
        setProducts(data.products);
        setTotalAmount(data.totalCartAmount);
      });
  }, []);

  return (
    <>
      <div className="home">
        <div className="cart-container">
          <div className="cart-header">
            <h5 className="heading">
              Shopping<span> Cart</span>
            </h5>
            <h6 className="action">remove all</h6>
          </div>

          {products &&
            products.map((product) => (
              <>
                <div className="cart-items col-12">
                  <div className="col-2">
                    <img className="img-cart" src={Product01} alt="" />
                  </div>
                  <di className="col-2">
                    <h3 className="title mt-5">{product.name}</h3>
                  </di>

                  <div className="counter col-2">
                    <div className="btn-cart">+</div>
                    <div className="count">{product.qty}</div>
                    <div className="btn-cart">-</div>
                  </div>
                  <div className="prices amountCart col-2">
                    <span id="peso">&#8369; </span>
                    {product.price}
                  </div>
                  <div className="col-1 remove">
                    <u>Remove</u>
                  </div>
                </div>
              </>
            ))}
              <br/>
              <div className="col-12 totalCartAmount">
                      Total Amount <span id="peso1">&#8369; </span>{totalAmount}
                      <br/><Button className="submit">Checkout</Button>
                      
                      </div>
             </div>

        </div>
        
    </>
  );
};

export default ViewCart;
