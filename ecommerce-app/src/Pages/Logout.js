import React, { useContext, useEffect } from 'react'
import { Navigate } from 'react-router-dom'
import UserContext from '../UserContext';

const Logout = () => {

 const { unsetUser, setUser } = useContext(UserContext);

 
 unsetUser();



  return (
    <Navigate to="/home" />
  )
}

export default Logout