import React from "react";
import { Link } from "react-router-dom";
import AboutImage from "../assets/products/about_img.png";
import './About.css'

const About = () => {
  return (
    <>
      <div className="about" id="about">

        <div className="about-image">
          <img src={AboutImage} alt="" />
        </div>

        <div className="content">
          <h3>premium <span>pet food</span> manufacturer</h3>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi
            eveniet modi omnis repellat tempora sint, inventore dolores
            necessitatibus, facere ullam harum quam sapiente alias doloremque
            praesentium id fugiat quo? Maiores?
            <br/>
            <Link to={"/shop"} className="btn" id="btn">read more</Link>
          </p>
        </div>

      </div>
    </>
  );
};

export default About;
