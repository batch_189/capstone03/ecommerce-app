import React, { useContext } from "react";
import shopNowBone from "../assets/products/shop_now_dog.png";
import shopNowFish from "../assets/products/shop_now_cat.png";
import dogFood from "../assets/products/dog_food.png";
import catFood from "../assets/products/cat_food.png";
import ProductList from "../Components/ProductList";
import './Shop.css'
import CartList from "./CartList";
import UserContext from "../UserContext";

const Shop = () => {
  const { user, setUser } = useContext(UserContext);
  return (
    <>
     <div className="dog-food">

      <div className="image">
        <img src={dogFood} alt="" />
      </div>

      <div className="content">
        <h3><span>air dried</span> dog food</h3>
        <p className="description">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi eveniet
          modi omnis repellat tempora sint, inventore dolores necessitatibus,
          facere ullam harum quam sapiente alias doloremque praesentium id
          fugiat quo? Maiores?
        </p>
        <div className="amount">Php 300.00 - 500.00</div>
        <a href="./OurProducts">
        <img src={shopNowBone} className="shopNow" alt=""/>
        </a>
      </div>
    </div>

    <div className="cat-food">
      
      <div className="content">
        <h3><span>air dried</span> cat food</h3>
        <p className="description">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi eveniet
          modi omnis repellat tempora sint, inventore dolores necessitatibus,
          facere ullam harum quam sapiente alias doloremque praesentium id
          fugiat quo? Maiores?
        </p>
        <div className="amount">Php 350.00 - 550.00</div>
       <a href="/">
        <img src={shopNowFish} className="shopNow"  alt=""/>
        </a>
      </div>
      <div className="image">
        <img src={catFood} alt="" />
      </div>
    </div>

    {user && !user.isAdmin  &&  <CartList />}

    {user && user.isAdmin && <ProductList />}
   
   
    </>
  );
};

export default Shop;
