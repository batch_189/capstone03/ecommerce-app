import React from "react";
import { Card, Container, Row, Col } from "react-bootstrap";
import Swal from "sweetalert2";
import product03 from "../assets/products/product_03.jpg";
import "./Product.css";

const CartView = (productProps) => {

  function addToCart (){
    console.log(productProps);
    console.log(productProps.productProps._id)
    fetch('http://localhost:5000/orders/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        productId: productProps.productProps._id,
        qty: 1
      })
    })
    .then((response) => response.json())
    .then((data) => {
      console.log(data)

      if(data !== undefined){

        Swal.fire({
          title: "Added to Cart",
          icon: "success",
        })
      } else {
        Swal.fire({
          title: "Something went wrong",
          icon: "error",
          text: "Please try again!",
        });
      }
    })
  }

  // const { name, description, stock, price } = productProps
  console.log(productProps)
  return (
    <>
      <Container className="product" id="product">
        <Row className="product-container d-inline-block mt-5 ml-5 mr-3">
          <Col lg={12} sm={12} md={6}>
            <Card className="box">
              <div className="icons">
               
                  <i className="fa fa-shopping-cart mr-3" onClick={addToCart}></i>
                
                  <i className="fa fa-heart mr-3"></i>
              
                  <i className="fa fa-eye"></i>
             
              </div>

              <div className="image-product">
                <img className="img-product" src={product03} alt="" />
              </div>

              <div className="productContent">
                <Card.Title className="name">{productProps.productProps.name}</Card.Title>
                <Card.Subtitle className="description">{productProps.productProps.description}</Card.Subtitle>
                <Card.Text className="stock"><span>Qty: </span>{productProps.productProps.stock}</Card.Text>
                <Card.Text className="amountP text-center"><span id="peso2">&#8369; </span>{productProps.productProps.price}</Card.Text>
              </div>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default CartView;
