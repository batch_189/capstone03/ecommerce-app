import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import ProductCards from "../Pages/ProductCards";
import CartView from "./CartView";
import "../Pages/Product.css";

export default function CartList() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch("http://localhost:5000/products/")
      .then((response) => response.json())
      .then((data) => {
        console.log(data);

        setProducts(
          data.map((product) => {
            return <CartView key={product._id} productProps={product} />;
          })
        );
      });
  }, []);

  return (
    <>
      <h1 className="text-center">
        <span>Our</span> Product
      </h1>
      <div className="goCartBtn">
        <Link className="sm" to={"/cart"} id="goToCart-btn">
          go to cart
        </Link>
      </div>
      {products}
    </>
  );
}
