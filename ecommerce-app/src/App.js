import { useState, useEffect } from "react";
import { Container } from "react-bootstrap";
import { UserProvider } from "./UserContext"
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import TopNav from "./Components/TopNavbar/TopNav";
import Home from "./Pages/Home";
import Shop from "./Pages/Shop";
import About from "./Pages/About";
import Admin from "./Components/forms/Admin";
import CartCard from "./Pages/CartCard";
import Contact from "./Pages/Contact";
import "./App.css";

const App = () => {

  const [user, setUser] = useState();

  return (
    <UserProvider value={{ user, setUser }}>
      <Router>
        <TopNav />
        <Container>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/about" element={<About />} />
            <Route path="/adminDashboard" element={<Admin />} />
            <Route path="/shop" element={<Shop />} />
            <Route path="/contact" element={<Contact />} />
            <Route path="/cart" element={<CartCard />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
};

export default App;
