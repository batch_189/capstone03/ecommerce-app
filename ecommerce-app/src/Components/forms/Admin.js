import React, { useState } from "react";
import { Form, Col, Row, Button } from "react-bootstrap";
import { useNavigate, Link } from "react-router-dom"
import Swal from "sweetalert2";
import ProductCards from "../../Pages/ProductCards";
import "./AdminForm.css";

const Admin = () => {

  const navigate = useNavigate();

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [stock, setStock] = useState("");
  const [price, setPrice] = useState("");

  function addProduct() {

    console.log("aaaaa");
    fetch('http://localhost:5000/products/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem("token")}`
      },
      body: JSON.stringify({
        name: name,
        description: description,
        stock: stock,
        price: price
      })
    })
    .then((response) => response.json())
    .then((data) => {
      console.log(data);

      if(data === true){
        Swal.fire({
          title: "Product Added Successfully",
          icon: "success",
          text: "Go to shop to check your product list or click add product to add more"
        });
      } else {
        Swal.fire({
          title: "Something went wrong",
          icon: "error",
          text: "Please try again!",
        });
      }
    })
    setName("");
    setDescription("");
    setStock("");
    setPrice("");
  }

  return (
    <div  className="home">
    <div className="formContainer">
      <div className="formTitle"><span>hello, </span>admin</div>

      <Form className="productForm">
        <Form.Label><h5>create product</h5></Form.Label>
        <Form.Group as={Row} className="adminFormBox" controlId="productName">

          <Form.Label column lg="1" className="formLabel">
            name:
          </Form.Label>
          <Col>
            <Form.Control 
            type="text"
            placeholder="Product name"
            onChange={(e) => {
              setName(e.target.value)
            }}
            />
          </Col>
        </Form.Group>

        <Form.Group as={Row} className="adminFormBox" controlId="productDescription">
          <Form.Label column lg="1" className="formLabel">
            description:
          </Form.Label>
          <Col>
            <Form.Control 
            as="textarea"
            row={3}
            placeholder="Product description"
            onChange={(e) => {
              setDescription(e.target.value)
            }}
            />
          </Col>
        </Form.Group>

        <Form.Group as={Row} className="adminFormBox" controlId="productStock">
          <Form.Label column lg="1" className="formLabel">
            stock:
          </Form.Label>
          <Col>
            <Form.Control 
            type="number" 
            placeholder="0"
            onChange={(e) => {
              setStock(e.target.value)
            }}
            />
          </Col>
        </Form.Group>

        <Form.Group as={Row} className="adminFormBox" controlId="productPrice">
          <Form.Label column lg="1" className="formLabel">
            price:
          </Form.Label>
          <Col>
            <Form.Control 
            type="number"
            onChange={(e) => {
              setPrice(e.target.value)
            }}
            placeholder="0.00"
            />
          </Col>
        </Form.Group>
        
        <div className="submitProductBtn">
        <Button
          className="submit mt-3 ml-3"
          size="md"
          type="submit"
          id="productFormBtn"
          onClick={addProduct}
        >
          Add Product
        </Button>

        <Button
          className="submit mt-3 ml-3"
          size="md"
          type="submit"
          id="productFormBtn"
          href={"/shop"}
        >
          See Shop
        </Button>
        </div>
      </Form>
    </div>
    </div>
  );
};

export default Admin;
