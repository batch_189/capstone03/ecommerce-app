import { React, useState, useContext, useEffect } from "react";
import { Form, Modal, Button } from "react-bootstrap";
import {  useNavigate } from "react-router-dom";
import UserContext from "../../UserContext";
import Swal from "sweetalert2";
import "./RegisterStyle.css"

const Login = () => {

  const {user, setUser} = useContext(UserContext) 

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const navigation = useNavigate()

  const [isActive, setIsActive] = useState(false);

  function validate(e) {
    e.preventDefault();
    console.log("validatevalidatevalidate");
    
    fetch("http://localhost:5000/users/login", {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({  
        email: email,
        password: password,
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        
        if(typeof data.access !== "undefined"){
          localStorage.setItem("token", data.access)
          retrieveUserDetails(data.access)
          console.log(data + "login")

        Swal.fire({
          title: "Login Successful",
          icon: "success",
          text: "Welcome to PawShop!"
          
        }).then(function() {
          document.getElementById("test").style.display = 'none';
          navigation("/Shop", {state: {user: user}})
        });

        } else{
          
          Swal.fire({
            title: "Authentication Failed",
            icon: "question",
            text: "Check your login details and try again!"
          })
        }
      });
    setEmail("");
    setPassword("");
  };

  const retrieveUserDetails = async (token) => {
   await fetch ('http://localhost:5000/users/details', {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    
    .then(response => response.json())
    .then(data => {
      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      });
      console.log("user: " + user);
    })
  }
  useEffect(()  => {

    if(
        email !== "" &&
        password !== "" 
    ) {
        setIsActive(true)
    } else {
        setIsActive(false)
    }
  }, [email, password]);


  return (

    <Modal.Body>
      <Form className="loginForm" id="login">
        <h5>Log In</h5>
        <Form.Group className="mb-3" controlId="emailForm"></Form.Group>
        <i className="fa fa-user mr-2"></i>
        <Form.Label>Enter your Email</Form.Label>
        <Form.Control
          type="email"
          placeholder="name@example.com"
          onChange={(e) => {
            setEmail(e.target.value);
          }}
          required
          autoFocus
        />
        <Form.Group className="mb-3" controlId="password"></Form.Group>
        <i className="fa fa-key mr-2"></i>
        <Form.Label>Enter your Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="enter your password"
          onChange={(e) => {
            setPassword(e.target.value);
          }}
          required
          autoFocus
        />
      </Form>
      <Form.Check className="checkbox" type="checkbox" label="Remember me" />
      {isActive ? (
         <Button className="submit mt-3 ml-3" size="md" type="submit" onClick={validate}>
         Submit
       </Button>
      ) : (
        <Button variant="danger" className="submit mt-3 ml-3" size="md" type="submit" disabled>
        Submit
      </Button>
      )
    }
     
    </Modal.Body>
  );
};

export default Login;
