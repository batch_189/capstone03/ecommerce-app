import React, { useContext, useEffect, useState } from "react";
import { Form, Modal, Button } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../../UserContext";
import "./RegisterStyle.css";

const Register = () => {

  const {user, setUser} = useContext(UserContext) 
  const navigation = useNavigate();

  const [email, setEmail] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");

  const [isActive, setIsActive] = useState(false);

  function registerUser(e) {
    e.preventDefault();

    fetch("http://localhost:5000/users/checkEmail", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        console.log("aaaaaa");

        if (data === true) {
          Swal.fire({
            title: "Email Match Found",
            icon: "error",
            text: "Kindly provide a new email to complete the registration.",
          });
        } else {
          console.log(
            JSON.stringify({
              email: email,
              password: password1,
            })
          );

          fetch("http://localhost:5000/users/", {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              email: email,
              password: password1,
            }),
          })
            .then((response) => response.json())
            .then((data) => {
              console.log(data);

              if (typeof data.access !== "undefined") {
                console.log(data.access)
                
                localStorage.setItem("token", data.access)
                retrieveUserDetails(data.access);
                document.getElementById("test").style.display = 'none';
                navigation("/Shop", {state: {user: user}})
                
              } else {
                Swal.fire({
                  title: "Something went wrong",
                  icon: "error",
                  text: "Please try again!",
              });
            }
          });
        }
      });
  };

  const retrieveUserDetails = async (token) => {
    await fetch ('http://localhost:5000/users/details', {
       headers: {
         Authorization: `Bearer ${token}`
       }
     })
     
     .then(response => response.json())
     .then(data => {
       setUser({
         id: data._id,
         isAdmin: data.isAdmin
       });
       console.log("user: " + user);
     })
   }

  useEffect(() => {
    if (
      email !== "" &&
      password1 !== "" &&
      password2 !== "" &&
      password1 === password2
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password1, password2]);


  return (
    <Modal.Body>
      <Form className="registerStyle" id="register">
        <h5>Register</h5>
        <Form.Group className="mb-3" controlId="emailForm">
          <i className="fa fa-user mr-2"></i>
          <Form.Label>Enter your Email</Form.Label>
          <Form.Control
            type="email"
            placeholder="name@example.com"
            value={email}
            onChange={(e) => {
              setEmail(e.target.value);
            }}
            required
            autoFocus
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="password1">
          <i className="fa fa-key mr-2"></i>
          <Form.Label>Enter your Password</Form.Label>
          <Form.Control
            type="password1"
            placeholder="enter your password"
            value={password1}
            onChange={(e) => {
              setPassword1(e.target.value);
            }}
            required
            autoFocus
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="password2">
          <i className="fa fa-key mr-2"></i>
          <Form.Label>Verify Password</Form.Label>
          <Form.Control
            type="password2"
            placeholder="Verify your password"
            value={password2}
            onChange={(e) => {
              setPassword2(e.target.value);
            }}
            required
            autoFocus
          />
        </Form.Group>

        {isActive ? (
           <Button
           className="submit mt-3 ml-3"
           size="md"
           type="submit"
           id="submit-btn"
           onClick={registerUser}
         >
           Submit
         </Button>
        ) : (
          <Button
          variant="danger"
          className="submit mt-3 ml-3"
          size="md"
          type="submit"
          id="submit-btn"
         disabled
        >
          Submit
        </Button>
        )}
       
      </Form>
    </Modal.Body>
  );
};
export default Register;
