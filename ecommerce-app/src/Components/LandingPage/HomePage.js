import React from 'react';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import BottomPicture from '../../assets/home/bottom_wave.png';
import Shop from "../../Pages/Shop"
import './HomePage.css';

const HomePage = () => {
  return (
    
    <div className='home'>
      <div className='homeContent'>
        <h4 className='home-text'><span className='hi'>Hi, </span>Welcome to our PawShop</h4>
        <Button href={"/shop"} className='home-btn' id="home-btn" >Shop Now</Button>
      </div>
      <img src={BottomPicture} className="wave" alt="bottomStyle"/>
    </div>
    
  );
};

export default HomePage;