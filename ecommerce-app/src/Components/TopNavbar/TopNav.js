import React, { useContext, useState } from "react";
import { Link } from "react-router-dom";
import { Navbar, Container, Nav, Modal, Button } from "react-bootstrap";
import Login from "../forms/Login";
import Register from "../forms/Register";
import Admin from "../forms/Admin";
import UserContext from "../../UserContext";
import "./TopNav.css";
import "./Login.css";

const TopNav = () => {
  const { user, setUser } = useContext(UserContext);

  const [showWindow, setShowWindow] = useState(false);
  const [showLogin, setShowLogin] = useState(false);
  const [showRegister, setShowRegister] = useState(false);
  const displayWindow = () => {
    setShowWindow(true);
    displayLogin();
    setShowRegister(false);
  };

  const displayRegister = () => {
    setShowRegister(true);
    setShowLogin(false);
  };
  const displayLogin = () => {
    setShowLogin(true);
    setShowRegister(false);
  };

  const closeWindow = () => {
    setShowWindow(false);
    setShowLogin(false);
    setShowRegister(false);
  };

  console.log("user: " + user)

  const submitRegister = () => {
    document.getElementById("register").submit();
  };

  const logout = () => {
    console.log("user: " + user);
    localStorage.clear();
    setUser(null);
  };
  console.log("ioioio" + user)

  const token = localStorage.getItem("token");
  console.log(token)
  if (token !== null) {
    loadToken(token);
  }

  function loadToken(token) {
     fetch ('http://localhost:5000/users/details', {
       headers: {
         Authorization: `Bearer ${token}`
       }
     })
     
     .then(response => response.json())
     .then(data => {
       setUser({
         id: data._id,
         isAdmin: data.isAdmin
       });
       console.log("user: " + user);
     })
   };
  return (
    <div>
      <Navbar className="navbar" expand="lg">
        <Container>
          <a href="/">
            <i className="fa fa-paw fa-2x" id="logo">
              <span className="brand-name">PawShop</span>
            </i>
          </a>
          <Navbar.Toggle className="toggler" aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ms-auto">      
              <li className="link">
              <Nav.Link href="/">Home</Nav.Link>
            </li>
            <li className="link">
              <Nav.Link href="/Shop">Shop</Nav.Link>
            </li>
            <li className="link">
              <Nav.Link href="/about">About</Nav.Link>
            </li>
            <li className="link">
              <Nav.Link href="/contact">Contact</Nav.Link>
            </li>
            {user && !user.isAdmin  && <li className="link">
             <Nav.Link href="/cart">Cart</Nav.Link>
            </li>}

            {user && user.isAdmin && 
            <>
            <li className="link">
                <Nav.Link href="/adminDashboard">Admin</Nav.Link>
            </li>
            </>
            }
            </Nav>
          </Navbar.Collapse>

          {!user && (
            <div>
              <i
                className="fa fa-user fa-2x"
                onClick={displayWindow}
                id="login-btn"
              ></i>
            </div>
          )}

          {user && (
            <div>
              <Link as={Link} id="login-btn" onClick={logout} to="/">
                <i className="fa fa-sign-out fa-2x"></i>
              </Link>
            </div>
          )}

           <Modal id="test" size="lg" show={showWindow} onHide={closeWindow}>
            <Modal.Header closeButton>
              <Modal.Title>
                <i className="fa fa-paw pr-2"></i>PawShop
              </Modal.Title>
            </Modal.Header>

            {showLogin && <Login/>}
            {showRegister && <Register />}

            <Modal.Footer className="user-btn mt-3 mr-3">
              <Link to="/" className="forgotP ml-5">
                Forgot Password
              </Link>

              {showLogin && (
                <Button
                  className="signUp-btn"
                  size="md"
                  type="submit"
                  onClick={displayRegister}
                >
                  Register
                </Button>
              )}

              {showRegister && (
                <Button
                  className="signUp-btn"
                  size="md"
                  type="submit"
                  onClick={displayLogin}
                >
                  Login
                </Button>
              )}
            </Modal.Footer>
          </Modal>
        </Container>
      </Navbar>
    </div>
  );
};

export default TopNav;
